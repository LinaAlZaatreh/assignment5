using System.Drawing;
using System.Drawing.Imaging;

namespace Assignment5
{
    abstract class ImageMorphology: IDisposable
    {
        private Bitmap _bitmap;
        private bool _disposed ;

        public ImageMorphology(Bitmap bitmap)
        {
            this._bitmap = bitmap;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _bitmap.Dispose();
                }

                _disposed = true;
            }
        }

        ~ImageMorphology()
        {
            Dispose(false);
        }

        static void Main(string[] args)
        {
            string directory = Path.GetDirectoryName(Directory.GetCurrentDirectory());
            string imagePath = Path.Combine(directory, "Converted.png");
            
            Bitmap originalImage = new Bitmap(imagePath);
            
            if (originalImage.PixelFormat!=PixelFormat.Format8bppIndexed)
            {
                Console.WriteLine("Image not in 8bpp format");
                return;
            }
            
            Bitmap binaryImage = ConvertToBinary(originalImage,127);

            Bitmap dilatedImage = ImageDilation(binaryImage);
            dilatedImage.Save("Dilated.bmp", ImageFormat.Bmp);

            Bitmap erodedImage = ImageErosion(binaryImage);
            erodedImage.Save("Eroded.bmp", ImageFormat.Bmp);

            Console.WriteLine("Morphological Dilation and Erosion applied successfully.");
            
            originalImage.Dispose();
            binaryImage.Dispose();
            dilatedImage.Dispose();
            erodedImage.Dispose();
        }

        static Bitmap ConvertToBinary(Bitmap grayscaleImage, int threshold)
        {
            int width = grayscaleImage.Width;
            int height = grayscaleImage.Height;

            Bitmap binaryImage = new Bitmap(width, height, PixelFormat.Format8bppIndexed);
    
            ColorPalette palette = binaryImage.Palette;
            palette.Entries[0] = Color.FromArgb(0,0,0);
            palette.Entries[1] = Color.FromArgb(255,255,255);
            binaryImage.Palette = palette;

            BitmapData grayscaleData = grayscaleImage.LockBits(new Rectangle(0, 0, width, height),
                ImageLockMode.ReadOnly, PixelFormat.Format8bppIndexed);

            BitmapData binaryData = binaryImage.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.WriteOnly,
                PixelFormat.Format8bppIndexed);

            unsafe
            {
                byte* grayscaleScan0 = (byte*)grayscaleData.Scan0;
                byte* binaryScan0 = (byte*)binaryData.Scan0;

                int grayStride = grayscaleData.Stride;
                int binaryStride = binaryData.Stride;
                
                for (int y = 0; y < height; y++)
                {
                    byte* grayscalePointer = grayscaleScan0 + y * grayStride;
                    byte* binaryPointer = binaryScan0 + y * binaryStride;

                    for (int x = 0; x < width; x++)
                    {
                        if (grayscalePointer[x] < threshold)
                            binaryPointer[x] = 0;
                        else
                            binaryPointer[x] = 255;
                    }
                }
            }

            grayscaleImage.UnlockBits(grayscaleData);
            binaryImage.UnlockBits(binaryData);

            return binaryImage;
        }

        static Bitmap ImageDilation(Bitmap image)
        {
            int width = image.Width;
            int height = image.Height;
            
            Bitmap dilatedImage = new Bitmap(width, height, PixelFormat.Format8bppIndexed);

            ColorPalette palette = dilatedImage.Palette;
            palette.Entries[0] = Color.FromArgb(0,0,0);
            palette.Entries[1] = Color.FromArgb(255,255,255);
            dilatedImage.Palette = palette;

            BitmapData originalImageData = image.LockBits(new Rectangle(0, 0, width, height),
                ImageLockMode.ReadOnly, PixelFormat.Format8bppIndexed);

            BitmapData dilatedImageData = dilatedImage.LockBits(new Rectangle(0, 0, width, height),
                ImageLockMode.WriteOnly, PixelFormat.Format8bppIndexed);

            byte[,] element = new byte[3, 3]
            {
                { 0, 1, 0 },
                { 1, 1, 1 },
                { 0, 1, 0 }
            };

            int size = 3;
            byte max, colorValue;
            int radius = size / 2;
            int ir, jr;

            unsafe
            {
                for (int y = radius; y < dilatedImage.Height - radius; y++)
                {
                    byte* originalPointer = (byte*)originalImageData.Scan0 + (y * originalImageData.Stride);
                    byte* dilatedPointer = (byte*)dilatedImageData.Scan0 + (y * dilatedImageData.Stride);

                    for (int x = radius; x < dilatedImageData.Width - radius; x++)
                    {
                        max = 0;

                        for (int elementColumn = 0; elementColumn < size; elementColumn++)
                        {
                            ir = elementColumn - radius;
                            byte* tempPointer = (byte*)originalImageData.Scan0 +
                                                ((y + ir) * originalImageData.Stride);

                            for (int elementRow = 0; elementRow < size; elementRow++)
                            {
                                jr = elementRow - radius;

                                colorValue = tempPointer[x + jr];

                                if (max < colorValue)
                                {
                                    if (element[elementColumn, elementRow] == 1)
                                        max = colorValue;
                                }
                            }
                        }

                        dilatedPointer[x] = max;
                        originalPointer++;
                        dilatedPointer++;
                    }
                }
            }

            image.UnlockBits(originalImageData);
            dilatedImage.UnlockBits(dilatedImageData);

            return dilatedImage;
        }
        
        static Bitmap ImageErosion(Bitmap image)
        {
            int width = image.Width;
            int height = image.Height;

            Bitmap erodedImage = new Bitmap(width, height, PixelFormat.Format8bppIndexed);
            ColorPalette palette = erodedImage.Palette;
            palette.Entries[0] = Color.FromArgb(0,0,0);
            palette.Entries[1] = Color.FromArgb(255,255,255);
            erodedImage.Palette = palette;

            BitmapData originalImageData = image.LockBits(new Rectangle(0, 0, width, height),
                ImageLockMode.ReadOnly, PixelFormat.Format8bppIndexed);

            BitmapData erodedImageData = erodedImage.LockBits(new Rectangle(0, 0, width, height),
                ImageLockMode.WriteOnly, PixelFormat.Format8bppIndexed);

            byte[,] element = new byte[3, 3]
            {
                { 0, 1, 0 },
                { 1, 1, 1 },
                { 0, 1, 0 }
            };

            int size = 3;
            byte min, colorValue;
            int radius = size / 2;
            int ir, jr;

            unsafe
            {
                for (int y = radius; y < erodedImage.Height - radius; y++)
                {
                    byte* originalPointer = (byte*)originalImageData.Scan0 + (y * originalImageData.Stride);
                    byte* erodedPointer = (byte*)erodedImageData.Scan0 + (y * erodedImageData.Stride);

                    for (int x = radius; x < erodedImageData.Width - radius; x++)
                    {
                        min = 255;

                        for (int elementColumn = 0; elementColumn < size; elementColumn++)
                        {
                            ir = elementColumn - radius;
                            byte* tempPointer = (byte*)originalImageData.Scan0 +
                                                ((y + ir) * originalImageData.Stride);

                            for (int elementRow = 0; elementRow < size; elementRow++)
                            {
                                jr = elementRow - radius;

                                colorValue = tempPointer[x + jr];

                                if (min > colorValue)
                                {
                                    if (element[elementColumn, elementRow] == 1)
                                        min = colorValue;
                                }
                            }
                        }

                        erodedPointer[x] = min;
                        originalPointer++;
                        erodedPointer++;
                    }
                }
            }

            image.UnlockBits(originalImageData);
            erodedImage.UnlockBits(erodedImageData);

            return erodedImage;
        }
    }
}
